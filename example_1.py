"""Завдання 4"""
import math

a = input(
    "Виберіть операцію: "
    "\n1 - додавання, 2 - віднімання,"
    "\n3 - множення, 4 - ділення,"
    "\n5 - зведення в ступінь, 6 - квадратний корінь, \n7 - кубічний корінь,"
    "\n8- синус, 9 - косинус, 0 - тангенс\n"
)

if a == "1" or a == "2" or a == "3" or a == "4" or a == "5":
    x = input("Введіть перше число ")
    y = input("Введіть друге число ")
    if x.isdigit() and y.isdigit():
        x, y = int(x), int(y)
        if a == "1":
            z = x + y
            print(z)

        elif a == "2":
            z = x - y
            print(z)

        elif a == "3":
            z = x * y
            print(z)

        elif a == "4":
            z = x / y
            print(z)

        elif a == "5":
            z = x**y
            print(z)
    else:
        print("Некоректне значення")

elif a == "6" or a == "7" or a == "8" or a == "9" or a == "0":
    x = input("Введіть число ")
    if x.isdigit():
        x = int(x)
        if a == "6":
            z = x**0.5
            print(z)

        elif a == "7":
            z = x ** (1 / 3)
            print(z)

        elif a == "8":
            z = math.sin(x)
            print(z)

        elif a == "9":
            z = math.cos(x)
            print(z)

        elif a == "0":
            z = math.tan(x)
            print(z)
        else:
            print("Некоректне значення")

else:
    print("Недопустиме значення. Введіть допустимий номер математичної операції")



